#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 12:06:15 2021

@author: cristian
"""

import numpy as np
from scipy.interpolate import RegularGridInterpolator as rgi
import plotly.graph_objects as go
import plotly.io as pio
from plotly.subplots import make_subplots
import plotly.figure_factory as ff
import pandas as pd
import plotly.express as px
from scipy import fftpack


pio.templates.default = "simple_white"
#%% ---------------------------  Unit system ---------------------------------------------
#	Longitude
UM = 1.0
MM = 1e3*UM
CM = 10.0*MM
M = 1e6*UM
#	Surface
M2 = M*M
CM2 = CM**2
#   Volume 
CM3 = CM**3
#	Charge
EC = 1.0
C = 1.0/1.602176634e-19*EC

V=1.0
#	Mass
KG = 1.0
#	Time
S = 1.0
NS = S*1.0e-9
#	Derived
N = KG*M/S**2.0 #Newton
#
#   Magnetic
T = N*S/M/C

Ksi=8.988e9/12*N*M2/C**2.0 #Coulomb's K relative to silicon

#%% ---------------------------  PARAMETERS ---------------------------------------------
PPUM = 1.0/UM
R = 4.0*UM # Column radius
SPITCH = 50.0*UM
LPITCH = 50.0*UM
THICK = 150.0*UM
BPTHICK = 3.0*UM # backplane Thickness
TTHICK = THICK+BPTHICK # total Thickness
REF_LIM = 46.0*UM # n col end Thickness
LAYER_LIM = REF_LIM+BPTHICK # array n col end limit

DEP_VOLT_5050 = 0.91*V
VBIAS = 10.0*V
SCALE_5050 = VBIAS/DEP_VOLT_5050

#Mobilities:
mobE = 1400*CM2/V/S
mobH = 400*CM2/V/S
qeh = 1.602176634e-19*EC # electron charge in coulombs   


a = SPITCH / (SPITCH*PPUM+1) # Steps, a b c, one for each dimension
b = LPITCH / (LPITCH*PPUM+1)    
c = TTHICK / (TTHICK*PPUM+1)

#%% FUNCTIONS:
def electric_field(pixel_pot):
	
	# Generating pixel's mesh:
    Aex = np.arange(a/2.0-a, SPITCH+a, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
    Bex = np.arange(b/2.0-b, LPITCH+b, b) - LPITCH/2.0
    Cex = np.arange(c/2.0, TTHICK+c, c) - BPTHICK

    x_ev, y_ev, z_ev = np.meshgrid(Bex, Aex, Cex) # Mesh with extra layer for derivate pot.
    pot_ev = x_ev.copy()*0
    
    dim1,dim2,dim3 = np.shape(pixel_pot)
    print([dim1,dim2,dim3])
    for i in range(0, dim1):
        for j in range(0, dim2):
            for k in range(0, dim3):
                pot_ev[i+1][j+1][k] = pixel_pot[i][j][k] 
    #Walls
    pot_ev[0,:,:]=pot_ev[1,:,:]
    pot_ev[dim1+1,:,:]=pot_ev[dim1,:,:]
    pot_ev[:,0,:]=pot_ev[:,1,:]
    pot_ev[:,dim2+1,:]=pot_ev[:,dim2,:]
    pot_ev[:,:,dim3]=pot_ev[:,:,dim3-1]
    
    efield_x,efield_y,efield_z=np.gradient(pot_ev, a, b, c)
    print(efield_x.shape)
    return [efield_x[1:dim1+1,1:dim2+1,0:dim3], efield_y[1:dim1+1,1:dim2+1,0:dim3], efield_z[1:dim1+1,1:dim2+1,0:dim3]] 
        

def general_field(pixel_pot, Aex, Bex, Cex):
	
    x_ev, y_ev, z_ev = np.meshgrid(Bex, Aex, Cex) # Mesh with extra layer for derivate pot.
    pot_ev = x_ev.copy()*0
    
    dim1,dim2,dim3 = np.shape(pixel_pot)
    print([dim1,dim2,dim3])
    for i in range(0, dim1):
        for j in range(0, dim2):
            for k in range(0, dim3):
                pot_ev[i+1][j+1][k] = pixel_pot[i][j][k] 
    #Walls
    pot_ev[0,:,:]=pot_ev[1,:,:]
    pot_ev[dim1+1,:,:]=pot_ev[dim1,:,:]
    pot_ev[:,0,:]=pot_ev[:,1,:]
    pot_ev[:,dim2+1,:]=pot_ev[:,dim2,:]
    pot_ev[:,:,dim3]=pot_ev[:,:,dim3-1]
    
    efield_x,efield_y,efield_z=np.gradient(pot_ev, a, b, c)
    print(efield_x.shape)
    return [efield_x[1:dim1+1,1:dim2+1,0:dim3], efield_y[1:dim1+1,1:dim2+1,0:dim3], efield_z[1:dim1+1,1:dim2+1,0:dim3]]     	


def inter3(xi, yi, zi, V, A, B, C):
    my_interpolating_function = rgi((A,B,C), V)
    return my_interpolating_function(np.array([xi,yi,zi]).T)

def inter2(xi, yi, V, A, B):
    my_interpolating_function = rgi((A,B), V)
    return my_interpolating_function(np.array([xi,yi]).T)

def streamline(x0, y0, z0, Ex, Ey, Ez, mask, A, B, C, move=0.01, steps=500):
    strX = np.array([])
    strY = np.array([])
    strZ = np.array([])
    strVal = np.array([])
    
    strX=np.append(strX,x0)
    strY=np.append(strY,y0)
    strZ=np.append(strZ,z0)
    
    Exi=inter3(strX[0], strY[0], strZ[0], Ex, A, B, C)
    Eyi=inter3(strX[0], strY[0], strZ[0], Ey, A, B, C)
    Ezi=inter3(strX[0], strY[0], strZ[0], Ez, A, B, C)
    mask_i = inter3(strX[0], strY[0], strZ[0], mask, A, B, C)
    
    strVal=np.append(strVal, np.sqrt(Exi**2+Eyi**2+Ezi**2))
    con = 0
    while con<steps and mask_i>0:  
        con += 1  
        normove = strVal[con-1]/move
        
        # Moving to the next point 
        strX=np.append(strX,strX[con-1]+Exi/normove)
        strY=np.append(strY,strY[con-1]+Eyi/normove)
        strZ=np.append(strZ,strZ[con-1]+Ezi/normove)     
        Exi=inter3(strX[con], strY[con], strZ[con], Ex, A, B, C)
        Eyi=inter3(strX[con], strY[con], strZ[con], Ey, A, B, C)
        Ezi=inter3(strX[con], strY[con], strZ[con], Ez, A, B, C)
        strVal=np.append(strVal,np.sqrt(Exi**2+Eyi**2+Ezi**2))
        
        mask_i = inter3(strX[con], strY[con], strZ[con], mask, A, B, C)
        
    
         
    return [strX,strY,strZ,strVal]

def streamline_ext(x0, y0, z0, Ex, Ey, Ez, mask, A, B, C, move=0.01, steps=500):
    """
    streamline (path, Efield values) of a particle moving inside a E field.
    
    streamline() generates the path of a particle moving inside an electric
    field. This is performed by taking small (pseudo-infinitesimal) steps, 
    interpolate the electric field and normalice the step to "move". This
    process stops if the particle reaches specific points defined by "mask" or
    if a maximum number of steps are computed (avoiding infinite loops).

    Parameters
    ----------
    x0, y0, z0 : float
        Initial coordinates to start the streamline generation.
    Ex, Ey, Ez : 3D float arrays
        Electric field components for the 3 dimensions.
    mask : 3D float array
        3D array with the stop function (1->continue, 0->stop).
    A, B, C : 1D float arrays
        Vectors that define the coordinates of "Ei" and "mask".
    move : float, optional
        Step (lenght). The default is 0.01.
    steps : int, optional
        Maximun number of steps that the function computes. The default is 500.

    Returns
    -------
    [strX,strY,strZ,strVal,strEx,strEy,strEz]
    strX, strY, strZ : 1D float arrays
        Vectors that define the 3Dimensional path of the particle.
    srtVal : 1D float arrays
        Electric field magnitude along the streamline.
    strEx, strEy, strEz : 1D float arrays
        Electric field contributions along the streamline.

    """
    
    strX = np.array([])
    strY = np.array([])
    strZ = np.array([])
    strVal = np.array([])
    strEx = np.array([])
    strEy = np.array([])
    strEz = np.array([])
    
    strX=np.append(strX,x0)
    strY=np.append(strY,y0)
    strZ=np.append(strZ,z0)
    
    Exi=inter3(strX[0], strY[0], strZ[0], Ex, A, B, C)
    Eyi=inter3(strX[0], strY[0], strZ[0], Ey, A, B, C)
    Ezi=inter3(strX[0], strY[0], strZ[0], Ez, A, B, C)
    mask_i = inter3(strX[0], strY[0], strZ[0], mask, A, B, C)
    
    strVal = np.append(strVal, np.sqrt(Exi**2+Eyi**2+Ezi**2))
    strEx = np.append(strEx,Exi)
    strEy = np.append(strEy,Eyi)
    strEz = np.append(strEz,Ezi)
    
    con = 0
    while con<steps and mask_i>0:
        try:
            con += 1  
            normove = strVal[con-1]/move
            
            # Moving to the next point 
            strX=np.append(strX,strX[con-1]+Exi/normove)
            strY=np.append(strY,strY[con-1]+Eyi/normove)
            strZ=np.append(strZ,strZ[con-1]+Ezi/normove)     
            Exi=inter3(strX[con], strY[con], strZ[con], Ex, A, B, C)
            Eyi=inter3(strX[con], strY[con], strZ[con], Ey, A, B, C)
            Ezi=inter3(strX[con], strY[con], strZ[con], Ez, A, B, C)
            strVal=np.append(strVal,np.sqrt(Exi**2+Eyi**2+Ezi**2))
            strEx = np.append(strEx,Exi)
            strEy = np.append(strEy,Eyi)
            strEz = np.append(strEz,Ezi)
            
            mask_i = inter3(strX[con], strY[con], strZ[con], mask, A, B, C)
        except:
            break
        
    return [strX,strY,strZ,strVal,strEx,strEy,strEz]

def streamline2D(x0, y0, Ex, Ey, mask, A, B, move=0.01, steps=500):
    strX = np.array([])
    strY = np.array([])
    strVal = np.array([])
    
    strX=np.append(strX,x0)
    strY=np.append(strY,y0)
    
    Exi=inter2(strX[0], strY[0], Ex, A, B)
    Eyi=inter2(strX[0], strY[0], Ey, A, B)
    mask_i = inter2(strX[0], strY[0], mask, A, B)
    
    strVal=np.append(strVal, np.sqrt(Exi**2+Eyi**2))
    con = 0
    while con<steps and mask_i>0:  
        con += 1  
        normove = strVal[con-1]/move
        
        # Moving to the next point 
        strX=np.append(strX,strX[con-1]+Exi/normove)
        strY=np.append(strY,strY[con-1]+Eyi/normove)    
        Exi=inter2(strX[con], strY[con], Ex, A, B)
        Eyi=inter2(strX[con], strY[con], Ey, A, B)
        strVal=np.append(strVal,np.sqrt(Exi**2+Eyi**2))
        
        mask_i = inter2(strX[con], strY[con], mask, A, B)
         
    return [strX,strY,strVal]


def ramo_signal(p0,Efields,EVecs,mask,Wfields,WVecs,l,maxlen,interval):
    """
    Induced signal by e-h pair generated
    
    ramo_signal() generates the individual electron-hole contributions and
    the total signal (current) as function of time by using the Ramo-Shockley
    theorem. It needs streamline() to compute the path of the particle, the
    velocity (prop. to mobility and Efields) and time. The final signal is 
    proportional to the velocity and is weigth through WFields. A step length 
    "l" must be settled and a maximun number of steps "maxlen" to avoid 
    infinity-vector generations. To sum signals the function finally interpolates
    the signal vectors into new vectors with t0=0 and tmax=interval, 2k steps.
    Then returns all, the interpolated and non-interpolated signals (t and I).

    Parameters
    ----------
    p0 : list (3 elements) -> float , p0=[p0x,p0y,p0z]
        p0 contains the 3 coordinates (3D) of the starter point for the 
        streamlines.
    Efields : list (3 elements) -> 3D float array, Efields=[Ex,Ey,Ez]
        Electric field spatial components Ex, Ey, Ez inside a pixels
        cell-like distribution .
    EVecs : list (3 elements) -> 1D float array, EVecs=[A,B,C]
        Evecs contains the 3 vectors that define the coordinates system of
        the electric field
    mask : 3D float array
        Mask to stop the streamline generations inside the electrodes.
    Wfields : List (3 elements) -> 3D float array, Wfields=[WEx,WEy,WEz]
        Weighting field spatial components WEx, WEy, WEz inside a pixels
        cell-like distribution .
    WVecs : list (3 elements) -> 1D float array, EVecs=[Aw,Bw,Cw]
        Evecs contains the 3 vectors that define the coordinates system of
        the weithing field (WVecs might be different to Evecs)
    l : float
        Step to advance and create the streamlines.
    maxlen : int
        Maximum number of steps to create the streamline. maxlen not defines
        the streamline length.
    interval : float
        Max value (seconds) to generate a linspace vector to interpole all the
        signals.

    Returns
    -------
    [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN]
    
    time_e: 1D float array
        Time vector for the electron signal generation
    signal_e: 1D float array
        Current vector for the electron signal generation 
    time_h: 1D float array
        Time vector for the hole signal generation
    signal_h: 1D float array
        Current vector for the hole signal generation 
    time_s: 1D float array
        Time vector for the total signal generation "signal"(interval) ,
        "sig_eN" and "sig_hN"
    signal: 1D float array
        Current vector for the Total signal generation interpolated in time_s
    sig_eN: 1D float array
        Current vector for the elec. signal generation interpolated in time_s
    sig_hN: 1D float array
        Current vector for the hole signal generation interpolated in time_s
    """
    # Electrons -> streamline to compute electron's path
    strX,strY,strZ,strVal,strEx,strEy,strEz = streamline_ext(
                                            p0[0], p0[1], p0[2], 
                                            Efields[0], Efields[1], Efields[2],
                                            mask, EVecs[0], EVecs[1], EVecs[2],
                                            l, maxlen)
    strVel = strVal*mobE # Velocity of the electrons UM/S 
    time_e = np.zeros_like(strVel) # Time vector for e-
    for i in range(1, time_e.shape[0]): # Time0 = 0 , losing 1pt
        time_e[i]=time_e[i-1]+l/strVel[i]
    
    strWEx = np.zeros_like(strEx) # Interpolated components of Weighting field
    strWEy = np.zeros_like(strEx) 
    strWEz = np.zeros_like(strEx)     
    for i in range(0, time_e.shape[0]):
        strWEx[i]=inter3(strX[i], strY[i], strZ[i], Wfields[0],
                         WVecs[0], WVecs[1], WVecs[2])
        strWEy[i]=inter3(strX[i], strY[i], strZ[i], Wfields[1], 
                         WVecs[0], WVecs[1], WVecs[2])
        strWEz[i]=inter3(strX[i], strY[i], strZ[i], Wfields[2], 
                         WVecs[0], WVecs[1], WVecs[2])
           
    strVelx = strEx*mobE # Velocity components
    strVely = strEy*mobE
    strVelz = strEz*mobE
    signal_e = qeh * (strVelx*strWEx+strVely*strWEy+strVelz*strWEz) # RAMO
    
    # Holes -> streamline to compute hole's path
    strX,strY,strZ,strVal,strEx,strEy,strEz = streamline_ext(
                                            p0[0], p0[1], p0[2], 
                                            -Efields[0], -Efields[1], -Efields[2],
                                            mask, EVecs[0], EVecs[1], EVecs[2],
                                            l, maxlen)
    strVel = strVal*mobH # Velocity of the holes UM/S 
    time_h = np.zeros_like(strVel) # Time vector for h+
    for i in range(1, time_h.shape[0]): # Time0 = 0 , losing 1pt
        time_h[i]=time_h[i-1]+l/strVel[i]
    
    strWEx = np.zeros_like(strEx) # Interpolated components of Weighting field
    strWEy = np.zeros_like(strEx) 
    strWEz = np.zeros_like(strEx)     
    for i in range(0, time_h.shape[0]):
        strWEx[i]=inter3(strX[i], strY[i], strZ[i], Wfields[0],
                         WVecs[0], WVecs[1], WVecs[2])
        strWEy[i]=inter3(strX[i], strY[i], strZ[i], Wfields[1], 
                         WVecs[0], WVecs[1], WVecs[2])
        strWEz[i]=inter3(strX[i], strY[i], strZ[i], Wfields[2], 
                         WVecs[0], WVecs[1], WVecs[2])
           
    strVelx = strEx*mobH # Velocity components
    strVely = strEy*mobH
    strVelz = strEz*mobH
    signal_h = -qeh * (strVelx*strWEx+strVely*strWEy+strVelz*strWEz) # RAMO
    
    # Signal (interpol) # Another way to interpole (to study)
    # maxArg=np.argmax([time_e[time_e.shape[0]-1],time_h[time_h.shape[0]-1]])
    # if maxArg == 0:
    #     time_s = time_e
    #     signal = signal_e + np.interp(time_e, time_h, signal_h, 0, 0)
    # else: 
    #     time_s = time_h
    #     signal = signal_h + np.interp(time_h, time_e, signal_e, 0, 0)
    time_s = np.linspace(0,interval,2000) # time vector to interpolate    
    sig_eN =  np.interp(time_s, time_e, signal_e, 0, 0) # Interpolated e- signal
    sig_hN = np.interp(time_s, time_h, signal_h, 0, 0) # Interpolated h+ signal
    signal =  sig_eN + sig_hN # Total signal   
    
    return [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN]

def ramo_magnetic(p0,Efields,EVecs,mask,Wfields,WVecs,BVec,l,maxlen,interval):
    """
    Induced signal by e-h pair generated under magnetic field
    
    ramo_magnetic() generates the individual electron-hole contributions and
    the total signal (current) as function of time by using the Ramo-Shockley
    theorem. It needs streamline() to compute the path of the particle, the
    velocity (prop. to mobility and Efields) and time. The final signal is 
    proportional to the velocity and is weigth through WFields. A step length 
    "l" must be settled and a maximun number of steps "maxlen" to avoid 
    infinity-vector generations. To sum signals the function finally interpolates
    the signal vectors into new vectors with t0=0 and tmax=interval, 2k steps.
    Then returns all, the interpolated and non-interpolated signals (t and I).

    Parameters
    ----------
    p0 : list (3 elements) -> float , p0=[p0x,p0y,p0z]
        p0 contains the 3 coordinates (3D) of the starter point for the 
        streamlines.
    Efields : list (3 elements) -> 3D float array, Efields=[Ex,Ey,Ez]
        Electric field spatial components Ex, Ey, Ez inside a pixels
        cell-like distribution .
    EVecs : list (3 elements) -> 1D float array, EVecs=[A,B,C]
        Evecs contains the 3 vectors that define the coordinates system of
        the electric field
    mask : 3D float array
        Mask to stop the streamline generations inside the electrodes.
    Wfields : List (3 elements) -> 3D float array, Wfields=[WEx,WEy,WEz]
        Weighting field spatial components WEx, WEy, WEz inside a pixels
        cell-like distribution .
    WVecs : list (3 elements) -> 1D float array, EVecs=[Aw,Bw,Cw]
        Evecs contains the 3 vectors that define the coordinates system of
        the weithing field (WVecs might be different to Evecs)
    BVec : list (3 elements) -> float, BVecs=[Bx,By,Bz]
        Magnetic field vector components.
    l : float
        Step to advance and create the streamlines.
    maxlen : int
        Maximum number of steps to create the streamline. maxlen not defines
        the streamline length.
    interval : float
        Max value (seconds) to generate a linspace vector to interpole all the
        signals.

    Returns
    -------
    [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN]
    
    time_e: 1D float array
        Time vector for the electron signal generation
    signal_e: 1D float array
        Current vector for the electron signal generation 
    time_h: 1D float array
        Time vector for the hole signal generation
    signal_h: 1D float array
        Current vector for the hole signal generation 
    time_s: 1D float array
        Time vector for the total signal generation "signal"(interval) ,
        "sig_eN" and "sig_hN"
    signal: 1D float array
        Current vector for the Total signal generation interpolated in time_s
    sig_eN: 1D float array
        Current vector for the elec. signal generation interpolated in time_s
    sig_hN: 1D float array
        Current vector for the hole signal generation interpolated in time_s
    """
    # New field defined --> E and B contributions: (e, h dependent)
    
    field_BEx = np.zeros_like(Efields[0])
    field_BEy = np.zeros_like(Efields[0])
    field_BEz = np.zeros_like(Efields[0])
    
    field_BEx = Efields[0] + mobE*(Efields[1]*BVec[2] - Efields[2]*BVec[1])
    field_BEy = Efields[1] + mobE*(Efields[0]*BVec[2] - Efields[2]*BVec[0])
    field_BEz = Efields[2] + mobE*(Efields[0]*BVec[1] - Efields[1]*BVec[0])
    
    # Electrons -> streamline to compute electron's path
    strX,strY,strZ,strVal,strEx,strEy,strEz = streamline_ext(
                                            p0[0], p0[1], p0[2], 
                                            field_BEx, field_BEy, field_BEz,
                                            mask, EVecs[0], EVecs[1], EVecs[2],
                                            l, maxlen)
    strVel = strVal*mobE # Velocity of the electrons UM/S 
    time_e = np.zeros_like(strVel) # Time vector for e-
    for i in range(1, time_e.shape[0]): # Time0 = 0 , losing 1pt
        time_e[i]=time_e[i-1]+l/strVel[i]
    
    strWEx = np.zeros_like(strEx) # Interpolated components of Weighting field
    strWEy = np.zeros_like(strEx) 
    strWEz = np.zeros_like(strEx)     
    for i in range(0, time_e.shape[0]):
        strWEx[i]=inter3(strX[i], strY[i], strZ[i], Wfields[0],
                         WVecs[0], WVecs[1], WVecs[2])
        strWEy[i]=inter3(strX[i], strY[i], strZ[i], Wfields[1], 
                         WVecs[0], WVecs[1], WVecs[2])
        strWEz[i]=inter3(strX[i], strY[i], strZ[i], Wfields[2], 
                         WVecs[0], WVecs[1], WVecs[2])
           
    strVelx = strEx*mobE # Velocity components
    strVely = strEy*mobE
    strVelz = strEz*mobE
    signal_e = qeh * (strVelx*strWEx+strVely*strWEy+strVelz*strWEz) # RAMO
    
    e_path = [strX,strY,strZ] # electron path
    
    # Holes -> streamline to compute hole's path
    
    field_BEx = np.zeros_like(Efields[0])
    field_BEy = np.zeros_like(Efields[0])
    field_BEz = np.zeros_like(Efields[0])
    
    field_BEx = -Efields[0] - mobH*(Efields[1]*BVec[2] - Efields[2]*BVec[1])
    field_BEy = -Efields[1] - mobH*(Efields[0]*BVec[2] - Efields[2]*BVec[0])
    field_BEz = -Efields[2] - mobH*(Efields[0]*BVec[1] - Efields[1]*BVec[0])
    
    
    strX,strY,strZ,strVal,strEx,strEy,strEz = streamline_ext(
                                            p0[0], p0[1], p0[2], 
                                            field_BEx, field_BEy, field_BEz,
                                            mask, EVecs[0], EVecs[1], EVecs[2],
                                            l, maxlen)
    
    strVel = strVal*mobH # Velocity of the holes UM/S 
    time_h = np.zeros_like(strVel) # Time vector for h+
    for i in range(1, time_h.shape[0]): # Time0 = 0 , losing 1pt
        time_h[i]=time_h[i-1]+l/strVel[i]
    
    strWEx = np.zeros_like(strEx) # Interpolated components of Weighting field
    strWEy = np.zeros_like(strEx) 
    strWEz = np.zeros_like(strEx)     
    for i in range(0, time_h.shape[0]):
        strWEx[i]=inter3(strX[i], strY[i], strZ[i], Wfields[0],
                         WVecs[0], WVecs[1], WVecs[2])
        strWEy[i]=inter3(strX[i], strY[i], strZ[i], Wfields[1], 
                         WVecs[0], WVecs[1], WVecs[2])
        strWEz[i]=inter3(strX[i], strY[i], strZ[i], Wfields[2], 
                         WVecs[0], WVecs[1], WVecs[2])
           
    strVelx = strEx*mobH # Velocity components
    strVely = strEy*mobH
    strVelz = strEz*mobH
    signal_h = -qeh * (strVelx*strWEx+strVely*strWEy+strVelz*strWEz) # RAMO
    
    # Signal (interpol) # Another way to interpole (to study)
    # maxArg=np.argmax([time_e[time_e.shape[0]-1],time_h[time_h.shape[0]-1]])
    # if maxArg == 0:
    #     time_s = time_e
    #     signal = signal_e + np.interp(time_e, time_h, signal_h, 0, 0)
    # else: 
    #     time_s = time_h
    #     signal = signal_h + np.interp(time_h, time_e, signal_e, 0, 0)
    time_s = np.linspace(0,interval,2000) # time vector to interpolate    
    sig_eN =  np.interp(time_s, time_e, signal_e, 0, 0) # Interpolated e- signal
    sig_hN = np.interp(time_s, time_h, signal_h, 0, 0) # Interpolated h+ signal
    signal =  sig_eN + sig_hN # Total signal   
    
    h_path = [strX,strY,strZ] # hole path
        
    return [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN, e_path, h_path]



#%% ---------------------------- FINAL FIGURES -------------------------------
#%% ---------------------------- WEIGHTING FIELD -----------------------------
# X Y Z are needed
loaded_arr = np.loadtxt("weighting_pot_50x50x150.txt")

A = np.arange(a/2.0, SPITCH*5, a) - SPITCH*5/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0, LPITCH*5, b) - LPITCH*5/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK

w_pot = loaded_arr.reshape(
    loaded_arr.shape[0], loaded_arr.shape[1] // x.shape[2], x.shape[2])

# Extended vectors
Aex = np.arange(a/2.0-a, SPITCH*5+a, a) - SPITCH*5/2.0 
Bex = np.arange(b/2.0-b, LPITCH*5+b, b) - LPITCH*5/2.0
Cex = np.arange(c/2.0, TTHICK+c, c) - BPTHICK

wFx, wFy, wFz = general_field(w_pot, Aex, Bex, Cex)
mask = np.zeros_like(wFx)
mask = (wFx != 0.0).astype(int)+(wFy != 0.0).astype(int)
mask = (mask > 0.0).astype(int)


pio.templates.default = "simple_white"
#%% 3D streamlines



Aplot = np.arange(0,120,12.5)
Bplot = np.arange(0,120,12.5)
Cplot = np.arange(1, TTHICK+c, c*30) - BPTHICK

fig = go.Figure()
pio.renderers.default = 'browser'
con=0
for i in range(0,Aplot.shape[0]):
    for j in range(0,Bplot.shape[0]):
        for k in range(0,Cplot.shape[0]):
            con=con+1
            print(con)
            strX,strY,strZ,strVal = streamline(Aplot[i], Bplot[j], Cplot[k],
                                               wFx, wFy, wFz,
                                       mask, A, B, C, 0.2, 3000)
            fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.5,
                                        mode='markers',
                                        marker=dict(
                                            size=12,
                                            color=strVal,                # set color to an array/list of desired values
                                            colorscale='Viridis',   # choose a colorscale
                                            colorbar=dict(thickness=20),
                                            opacity=0.8
                                            )))    
    
    

fig.update_layout(
    title="Charges drift under E field",
    coloraxis_colorbar=dict(title="[V/um]"),
    scene = dict(
        #xaxis = dict(nticks=4, range=[-25.01,25.01],),
        #yaxis = dict(nticks=4, range=[-25.01,25.01],),
        zaxis = dict(nticks=4, range=[0,149],),
        ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=1.5)
    )
fig.update_layout(scene_camera=camera)
fig.show()

#%% 3D random streamlines
nsampl=200
samplA = np.random.uniform(low=-100, high=100, size=(nsampl,))
samplB = np.random.uniform(low=-100, high=100, size=(nsampl,))
samplC = np.random.uniform(low=0.5, high=149, size=(nsampl,))

fig = go.Figure()
pio.renderers.default = 'browser'
con=0
for k in range(0,nsampl):
    con=con+1
    print(con)
    strX,strY,strZ,strVal = streamline(samplA[k], samplB[k], samplC[k],
                                       wFx, wFy, wFz,
                               mask, A, B, C, 0.2, 3000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.5,
                                mode='markers',
                                marker=dict(
                                    size=12,
                                    color=strVal,                # set color to an array/list of desired values
                                    colorscale='Viridis',   # choose a colorscale
                                    colorbar=dict(thickness=20),
                                    opacity=0.8
                                    )))    
    
    

fig.update_layout(
    title="Charges drift under E field",
    coloraxis_colorbar=dict(title="[V/um]"),
    scene = dict(
        #xaxis = dict(nticks=4, range=[-25.01,25.01],),
        #yaxis = dict(nticks=4, range=[-25.01,25.01],),
        zaxis = dict(nticks=4, range=[0,149],),
        ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=1.5)
    )
fig.update_layout(scene_camera=camera)
fig.show()

#%% 2D z-wiew isolines

fig = go.Figure()
pio.renderers.default = 'browser'
surf_cut100 = w_pot[:,:,100].copy()
mask_2D = mask[:,:,100].copy()
wFx_2D=wFx[:,:,100]
wFy_2D=wFy[:,:,100]


fig.add_trace(go.Contour(z=(surf_cut100),
                         dx=a,
                         x0=-125,
                         dy=b,
                         y0=-125,
                         colorscale='Inferno',
                            contours=dict(
                                start=0,
                                end=1,
                                size=0.1,
                            ),
                         colorbar=dict(
                             title="W potential <br> [a.u]",
                             tick0= -6,
                             tickmode= 'array',
                             tickvals= [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],
                             #ticktext=["1E-6","1E-5","1E-4","1E-3","1E-2","1E-1","0"]
                         )))

fig.update_layout(coloraxis_colorbar=dict(
    title="W potential <br> [a.u]",
    thicknessmode="pixels", thickness=50,
    lenmode="pixels", len=200,
    yanchor="top", y=1,
    ticks="outside", ticksuffix=" bills",
    dtick=5
))

fig.update_yaxes(ticks="inside",
                 scaleanchor = "x",
                 scaleratio = 1,
                 range=[-99.9,99.9])
fig.update_xaxes(ticks="inside", range=[-99.9,99.9])

fig.update_layout(scene = dict(
    ),
    title="Weighting potential, z=100um - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)

fig.write_image("Figures/wpot_isolines_5050.png")
fig.show()

#%% 2D z-wiew heatmap

fig = go.Figure()
pio.renderers.default = 'browser'
surf_cut100 = w_pot[:,:,100].copy()
mask_2D = mask[:,:,100].copy()
wFx_2D=wFx[:,:,100]
wFy_2D=wFy[:,:,100]


fig.add_trace(go.Heatmap(z=(surf_cut100),
                         dx=a,
                         x0=-125,
                         dy=b,
                         y0=-125,
                         colorscale='Inferno',
                            # contours=dict(
                            #     start=0,
                            #     end=1,
                            #     size=0.1,
                            # ),
                         colorbar=dict(
                             title="W potential <br> [a.u]",
                             tick0= -6,
                             tickmode= 'array',
                             tickvals= [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],
                             #ticktext=["1E-6","1E-5","1E-4","1E-3","1E-2","1E-1","0"]
                         )))

fig.update_layout(coloraxis_colorbar=dict(
    title="W potential <br> [a.u]",
    thicknessmode="pixels", thickness=50,
    lenmode="pixels", len=200,
    yanchor="top", y=1,
    ticks="outside", ticksuffix=" bills",
    dtick=5
))

fig.update_yaxes(ticks="inside",
                 scaleanchor = "x",
                 scaleratio = 1,
                 range=[-99.9,99.9])
fig.update_xaxes(ticks="inside", range=[-99.9,99.9])

fig.update_layout(scene = dict(
    ),
    title="Weighting potential, z=100um - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)

fig.write_image("Figures/wpot_heatmap_5050.png")
fig.show()


#%% 2D z-wiew isolines log

fig = go.Figure()
pio.renderers.default = 'browser'
surf_cut100 = w_pot[:,:,100].copy()
mask_2D = mask[:,:,100].copy()
wFx_2D=wFx[:,:,100]
wFy_2D=wFy[:,:,100]


fig.add_trace(go.Contour(z=np.log10(surf_cut100+1e-10),
                         dx=a,
                         x0=-125,
                         dy=b,
                         y0=-125,
                         colorscale='Inferno',
                         contours=dict(
                                start=-6,
                                end=0.1,
                                size=0.5,
                            ),
                         colorbar=dict(
                             title="W potential <br> [a.u]",
                             tick0= -6,
                             tickmode= 'array',
                             tickvals= [-6, -5, -4, -3, -2, -1, 0],
                             ticktext=["1E-6","1E-5","1E-4","1E-3","1E-2","1E-1","0"]
                         )))

fig.update_layout(coloraxis_colorbar=dict(
    title="W potential <br> [a.u]",
    thicknessmode="pixels", thickness=50,
    lenmode="pixels", len=200,
    yanchor="top", y=1,
    ticks="outside", ticksuffix=" bills",
    dtick=5
))

fig.update_yaxes(ticks="inside",
                 scaleanchor = "x",
                 scaleratio = 1,
                 range=[-99.9,99.9])
fig.update_xaxes(ticks="inside", range=[-99.9,99.9])

fig.update_layout(scene = dict(
    ),
    title="Weighting potential, z=100um - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)

fig.write_image("Figures/wpot_isolines_log_5050.png")
fig.show()

#%% 2D z-wiew paths
Aplot = np.arange(-120,120,10)+0.2
Bplot = np.arange(-120,120,10)+0.1


fig = go.Figure()
pio.renderers.default = 'browser'
x = [-25, 25, 25, -25, -25]
y = [-25, -25, 25, 25, -25]

fig.add_trace(go.Scatter(x=x, y=y,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='green',
                                    width=2,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        ))

con=0
showBar = True
for i in range(0,Aplot.shape[0]):
    for j in range(0,Bplot.shape[0]):
        con=con+1
        print(con)
        strX,strY,strVal = streamline2D(Aplot[i], Bplot[j],
                                            wFx_2D, wFy_2D, mask_2D, A, B, 0.2, 3000)
        fig.add_trace(go.Scatter(x=strX, y=strY, marker_size=2,
                                    mode='markers',
                                    showlegend = False,
                                    marker=dict(
                                        showscale=showBar,
                                        size=12,
                                        color=strVal,                # set color to an array/list of desired values
                                        colorscale='Viridis',   # choose a colorscale
                                        colorbar=dict(nticks=0,thickness=20),
                                        colorbar_title="W field <br> [a.u]",
                                        opacity=0.8
                                        ))) 
        if strX.shape[0]>0:
            showBar = False

pcolx = np.arange(-75,76,50)
pcoly = np.arange(-75,76,50) 

for i in range(0,pcolx.shape[0]):
    for j in range(0,pcoly.shape[0]):
        fig.add_trace(go.Scatter(x=np.array(pcolx[i]), y=np.array(pcoly[j]), marker_size=5,
                                    mode='markers',
                                    showlegend = False,
                                    marker=dict(
                                        size=12,
                                        color="blue",
                                        )))  
        
ncolx = np.arange(-50,51,50)
ncoly = np.arange(-50,51,50) 

for i in range(0,ncolx.shape[0]):
    for j in range(0,ncoly.shape[0]):
        fig.add_trace(go.Scatter(x=np.array(ncolx[i]), y=np.array(ncoly[j]), marker_size=5,
                                    mode='markers',
                                    showlegend = False,
                                    marker=dict(
                                        size=12,
                                        color="red",
                                        )))  
        
fig.update_yaxes(ticks="inside",
                 scaleanchor = "x",
                 scaleratio = 1,
                 range=[-99.9,99.9])
fig.update_xaxes(ticks="inside", range=[-99.9,99.9])

fig.update_layout(scene = dict(
    ),
    title="Weighting field, z=100um - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)
fig.write_image("Figures/wfield_streamlines_5050.png")
fig.show()

#%% 2D zy-wiew isolines

fig = go.Figure()
pio.renderers.default = 'browser'
surf_cut100 = w_pot[127,:,:].copy()


fig.add_trace(go.Contour(z=(surf_cut100.T),
                         dx=a,
                         x0=-125,
                         dy=c,
                         y0=-3,
                         colorscale='Inferno',
                            contours=dict(
                                start=0,
                                end=1,
                                size=0.1,
                            ),
                         colorbar=dict(
                             title="W potential <br> [a.u]",
                             tick0= -6,
                             tickmode= 'array',
                             tickvals= [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1],
                             #ticktext=["1E-6","1E-5","1E-4","1E-3","1E-2","1E-1","0"]
                         )))

fig.update_layout(coloraxis_colorbar=dict(
    title="W potential <br> [a.u]",
    thicknessmode="pixels", thickness=50,
    lenmode="pixels", len=200,
    yanchor="top", y=1,
    ticks="outside", ticksuffix=" bills",
    dtick=5
))

fig.update_yaxes(ticks="inside",
                 #scaleanchor = "x",
                 #scaleratio = 1,
                 range=[-2.5,149.9])
fig.update_xaxes(ticks="inside", range=[-99.9,99.9])

fig.update_layout(scene = dict(
    ),
    title="Weighting potential, YZ plane - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)

fig.write_image("Figures/wpot_isolinesXY_5050.png")
fig.show()

#%% 2D zy-wiew paths no work!!!
#empty
#%% 1D

fig = go.Figure()
pio.renderers.default = 'browser'

fig.add_trace(go.Scatter(x=[0,0], y=[0,1.1], mode='lines', line_shape='linear', showlegend = False,
                            line = dict(color='black',  width=1.5)))
fig.add_trace(go.Scatter(x=[25,25], y=[0,1.1], mode='lines', line_shape='linear', showlegend = False,
                            line = dict(color='red', dash='dot', width=1.5)))
fig.add_trace(go.Scatter(x=[-35.36,-35.36], y=[0,1.1], mode='lines', line_shape='linear', showlegend = False,
                            line = dict(color='red', dash='dot',  width=1.5)))

# Alpha
con=0
for i in [103,49,48,43,33]:
    v_alfaX = A[127:255]
    v_alfaY = w_pot[127,127:255,i]
    linedash = ['longdash','solid', 'dash', 'dot', 'dashdot']
    legend_v = ["100","46","45","40","30"]
    
    
    
    
    fig.add_trace(go.Scatter(x=v_alfaX, y=v_alfaY,
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name=legend_v[con],
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash=linedash[con]
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))
    con = con+1


# Betta
con=0
for i in [103,49,48,43,33]:
    v_betaX = np.array([])
    v_betaY = np.array([])
    for j in range(127, 255):
        x=np.sqrt(A[j]**2+A[j]**2)
        y=w_pot[j,j,i]
        v_betaX=np.append(v_betaX,x)
        v_betaY=np.append(v_betaY,y)
    
    linedash = ['longdash','solid', 'dash', 'dot', 'dashdot']
    
    
    
    
    fig.add_trace(go.Scatter(x=-v_betaX, y=v_betaY,
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="",
                            line_shape='linear', # 'spline'
                            showlegend = False,
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash=linedash[con]
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))
    con = con+1


    
fig.update_xaxes(ticks="inside", range=[-38,50], 
                 tickvals=[-35.36,0,25],
                 ticktext = [u"\u03B3","O",u"\u03B1"])
fig.update_yaxes(ticks="inside", range=[0,1.1])

fig.update_layout(legend=dict(
    title = "z [um]:",
    yanchor="top",
    y=0.99,
    xanchor="right",
    x=0.90),
    xaxis_title="",
    yaxis_title="Weighting pot. [a.u.]",)



fig.update_layout(scene = dict(
    ),
    title="Weighting potential, 1D - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18)
)
fig.write_image("Figures/wpot_1D_5050.png")
fig.show()

#%% ---------------------------- FINAL FIGURES -------------------------------
#%% ------------------------- ELECTRIC POTENTIAL -----------------------------

#%% Load Potential saved
# X Y Z are needed
loaded_arr = np.loadtxt("arrayPotSum_5050.txt")

Ap = np.arange(a/2.0, SPITCH, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
Bp = np.arange(b/2.0, LPITCH, b) - LPITCH/2.0
Cp = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(Bp, Ap, Cp) # Mesh x-y centered and z0=-BPTHICK
[dim1, dim2, dim3] = x.shape

arrayPotSum = loaded_arr.reshape(
    loaded_arr.shape[0], loaded_arr.shape[1] // x.shape[2], x.shape[2])

Ex,Ey,Ez = electric_field(arrayPotSum)

A = np.arange(a/2.0, SPITCH, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0, LPITCH, b) - LPITCH/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

# Generating pixel's mesh:
cells_x = 1
cells_y = 1
A = np.arange(a/2.0-SPITCH*cells_y, SPITCH+SPITCH*cells_y, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0-LPITCH*cells_x, LPITCH+LPITCH*cells_x, b) - LPITCH/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK
    
cells_Ex = np.zeros_like(x)
cells_Ey = np.zeros_like(x) 
cells_Ez = np.zeros_like(x)


for ix in [0, dim1, dim1*2]:
    for iy in [0, dim2, dim2*2]:
        cells_Ex[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ex
        cells_Ey[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ey
        cells_Ez[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ez
                         
    
#Defining mask
mask = np.ones_like(x)
for nx in range (-cells_x, cells_x+1): # Adding columns (n)
    for ny in range (-cells_y, cells_y+1):
        px = nx*LPITCH
        py = ny*SPITCH
        r = np.sqrt((px-x)**2+(py-y)**2)
        rmask = (r > R).astype(int)+(z < REF_LIM).astype(int)
        rmask = (rmask > 0).astype(int)
        mask = mask*rmask
    
for nx in range (-cells_x-1, cells_x+2): # Adding columns (p)
    for ny in range (-cells_y-1, cells_y+2):
        px = nx*LPITCH+LPITCH/2
        py = ny*SPITCH+SPITCH/2
        r = np.sqrt((px-x)**2+(py-y)**2)
        rmask = (r > R).astype(int)
        mask = mask*rmask
        
mask = mask*(z > 0).astype(int) * (z < THICK-c).astype(int)

efield_magnitude=np.sqrt(Ex**2+Ey**2+Ez**2)
maxVal=np.max(efield_magnitude)
minVal=np.min(efield_magnitude)

#%% Circle e-h generation

pio.renderers.default = 'browser'

fig = go.Figure()

radi=15
ptAng=np.linspace(0,2*np.pi,60)
ptx=radi*np.cos(ptAng)
pty=radi*np.sin(ptAng)
for i in range(0,ptx.shape[0]):
    
    strX,strY,strZ,strVal = streamline(ptx[i], pty[i], 100, cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.5,
                                        marker_color="blue",
                                        showlegend = False,
                                        mode='markers'))
    strX,strY,strZ,strVal = streamline(ptx[i], pty[i], 100, -cells_Ex, -cells_Ey, -cells_Ez,
                                       mask, A, B, C, 0.05, 1000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.5,
                                        marker_color="red",
                                        showlegend = False,
                                        mode='markers'))
    fig.add_trace(go.Scatter3d(x=np.array(ptx[i]), y=np.array(pty[i]),
                               z=np.array(100.0), marker_size=2,
                                        marker_color="orange",
                                        showlegend = False,
                                        mode='markers'))

fig.update_layout(
    scene = dict(
        xaxis = dict(range=[-25.01,25.01], tickvals=[]),
        yaxis = dict(range=[-25.01,25.01], tickvals=[]),
        zaxis = dict(range=[0,149],tickvals=[]),
        ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=2.3)
    )
fig.update_layout(scene_camera=camera)

fig.update_layout(scene = dict(
    ),
    title="Drift of e-h pairs - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18)
)
fig.write_image("Figures/elines_circle_eh.png")
fig.show()


#%% Circle E magnitude

fig = go.Figure()
radi=R+0.01
ptAng=np.linspace(0*np.pi,2*np.pi,60)
ptx=radi*np.cos(ptAng)
pty=radi*np.sin(ptAng)
for i in range(0,ptx.shape[0]):
    
    strX,strY,strZ,strVal = streamline(ptx[i], pty[i], 100, -cells_Ex, -cells_Ey, -cells_Ez,
                                       mask, A, B, C, 0.05, 1000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.5,
                                        mode='markers',
                                        showlegend = False,
                                        marker=dict(
                                            size=12,
                                            color=strVal,                # set color to an array/list of desired values
                                            colorscale='Viridis',   # choose a colorscale
                                            colorbar=dict(thickness=20),
                                            opacity=0.8
                                            )))    

fig.update_layout(
    scene = dict(
        xaxis = dict(range=[-25.01,25.01], tickvals=[]),
        yaxis = dict(range=[-25.01,25.01], tickvals=[]),
        zaxis = dict(range=[0,149],tickvals=[]),
        ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=2.3)
    )
fig.update_layout(scene_camera=camera)

fig.update_layout(scene = dict(
    ),
    title="Drift of e-h pairs - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18)
)
fig.write_image("Figures/elines_z100_magnitude.png")
fig.show()


#%% Random multiple streamline

pio.renderers.default = 'browser'

fig = go.Figure()
fig.add_trace(go.Isosurface(
    x=x.flatten(),
    y=y.flatten(),
    z=z.flatten(),
    value=efield_magnitude.flatten(),
    isomin=minVal,
    isomax=maxVal,
    surface_count=4,
    opacity=0.6,
    caps=dict(x_show=False, y_show=False)
    ))

data = fig._data

for n in range(100):
    rdn1 = np.random.randint(-24, 24 + 1)
    rdn2 = np.random.randint(-24, 24 + 1)
    rdn3 = np.random.randint(1, 149 + 1)
    strX,strY,strZ,strVal = streamline(rdn1, rdn2, rdn3, Ex, Ey, Ez, mask, A, B, C, 0.06, 2000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.33,
                                        marker_color="red",
                                        mode='markers'))
   
fig.update_layout(
    scene = dict(
        zaxis = dict(nticks=4, range=[0,149],),))
fig.show()

#%% Random multiple streamline

pio.renderers.default = 'browser'

fig = go.Figure()
fig.add_trace(go.Isosurface(
    x=x.flatten(),
    y=y.flatten(),
    z=z.flatten(),
    value=efield_magnitude.flatten(),
    isomin=minVal,
    isomax=maxVal,
    surface_count=4,
    opacity=0.6,
    caps=dict(x_show=False, y_show=False)
    ))

data = fig._data

for n in range(4, 153, 4):
    rdn1 = np.random.randint(-24, 24 + 1)
    rdn2 = np.random.randint(-24, 24 + 1)
    rdn3 = np.random.randint(1, 149 + 1)
    strX,strY,strZ,strVal = streamline(20, 20, C[n], Ex, Ey, Ez, mask, A, B, C, 0.06, 2000)
    fig.add_trace(go.Scatter3d(x=strX, y=strY, z=strZ, marker_size=0.33,
                                        marker_color="red",
                                        mode='markers'))
   
fig.update_layout(
    scene = dict(
        zaxis = dict(nticks=4, range=[0,149],),))
fig.show()

#%% Depletion voltage 50x50

plim = 25-R-0.01
#Diagonaly
disc = 100
zvec = np.linspace(0.1,149,disc)
xydiag = np.linspace(0,plim,disc)


dep_fie = np.array([])
   
for i in range(zvec.shape[0]): # Diagon #0
    strX,strY,strZ,strVal = streamline(plim, plim, zvec[i],
                                       cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)

    dep_fie = np.append(dep_fie, np.sum(strVal*0.05))
    
for i in range(zvec.shape[0]): # Colum border #1
    strX,strY,strZ,strVal = streamline(24.9, plim, zvec[i],
                                       cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)

    dep_fie = np.append(dep_fie, np.sum(strVal*0.05))    
    
for i in range(xydiag.shape[0]): # z=0 gamma #2
    strX,strY,strZ,strVal = streamline(xydiag[i], xydiag[i], 0.1,
                                       cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)

    dep_fie = np.append(dep_fie, np.sum(strVal*0.05))      

for i in range(xydiag.shape[0]): # z= 0 alfa #3
    strX,strY,strZ,strVal = streamline(0, xydiag[i], 0.1,
                                       cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)

    dep_fie = np.append(dep_fie, np.sum(strVal*0.05))   
    
for i in range(xydiag.shape[0]): # z= 0 border #4
    strX,strY,strZ,strVal = streamline(24.9, xydiag[i], 0.1,
                                       cells_Ex, cells_Ey, cells_Ez,
                                       mask, A, B, C, 0.05, 1000)

    dep_fie = np.append(dep_fie, np.sum(strVal*0.05)) 
       
depletion_voltage = np.max(dep_fie)
dep_indx = np.argmax(dep_fie)
print("depletion_voltage")
print(depletion_voltage)
print("Index")
print(dep_indx)
print("Zone")
print(dep_indx/disc)

#%% ---------------------------- FINAL FIGURES -------------------------------
#%% ------------------------- SIGNAL GENERATION -----------------------------

#%% Load files

loaded_arr = np.loadtxt("weighting_pot_50x50x150.txt")

A = np.arange(a/2.0, SPITCH*5, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0, LPITCH*5, b) - LPITCH/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK

w_pot = loaded_arr.reshape(
    loaded_arr.shape[0], loaded_arr.shape[1] // x.shape[2], x.shape[2])

loaded_arr = np.loadtxt("arrayPotSum_5050.txt")

Ap = np.arange(a/2.0, SPITCH, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
Bp = np.arange(b/2.0, LPITCH, b) - LPITCH/2.0
Cp = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(Bp, Ap, Cp) # Mesh x-y centered and z0=-BPTHICK
[dim1, dim2, dim3] = x.shape

arrayPotSum = loaded_arr.reshape(
    loaded_arr.shape[0], loaded_arr.shape[1] // x.shape[2], x.shape[2])

Ex,Ey,Ez = electric_field(arrayPotSum)

A = np.arange(a/2.0, SPITCH, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0, LPITCH, b) - LPITCH/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

cells_x = 1
cells_y = 1
A = np.arange(a/2.0-SPITCH*cells_y, SPITCH+SPITCH*cells_y, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
B = np.arange(b/2.0-LPITCH*cells_x, LPITCH+LPITCH*cells_x, b) - LPITCH/2.0
C = np.arange(c/2.0, TTHICK, c) - BPTHICK

x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK
    
cells_Ex = np.zeros_like(x)
cells_Ey = np.zeros_like(x) 
cells_Ez = np.zeros_like(x)


for ix in [0, dim1, dim1*2]:
    for iy in [0, dim2, dim2*2]:
        cells_Ex[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ex
        cells_Ey[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ey
        cells_Ez[ix:ix+dim1, iy:iy+dim2, 0:dim3] = Ez
  
                            
#Defining mask
mask = np.ones_like(x)
for nx in range (-cells_x, cells_x+1): # Adding columns (n)
    for ny in range (-cells_y, cells_y+1):
        px = nx*LPITCH
        py = ny*SPITCH
        r = np.sqrt((px-x)**2+(py-y)**2)
        rmask = (r > R).astype(int)+(z < REF_LIM).astype(int)
        rmask = (rmask > 0).astype(int)
        mask = mask*rmask
    
for nx in range (-cells_x-1, cells_x+2): # Adding columns (p)
    for ny in range (-cells_y-1, cells_y+2):
        px = nx*LPITCH+LPITCH/2
        py = ny*SPITCH+SPITCH/2
        r = np.sqrt((px-x)**2+(py-y)**2)
        rmask = (r > R).astype(int)
        mask = mask*rmask
        
mask = mask*(z > 0).astype(int) * (z < THICK-c).astype(int)


wFx,wFy,wFz = np.gradient(w_pot, a, b, c)
cells_x = 2
cells_y = 2
Aw = np.arange(a/2.0-SPITCH*cells_y, SPITCH+SPITCH*cells_y, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
Bw = np.arange(b/2.0-LPITCH*cells_x, LPITCH+LPITCH*cells_x, b) - LPITCH/2.0
Cw = np.arange(c/2.0, TTHICK, c) - BPTHICK

#%% Single E-H pair


p0 = [18,18,100]
Efields = [cells_Ex, cells_Ey, cells_Ez]
EVecs = [A, B, C]
Wfields = [wFx, wFy, wFz]
WVecs = [Aw, Bw, Cw]
l=0.05 #UM
maxlen = 3000
interval = 50e-9


time_e,signal_e,time_h,signal_h,time_s,signal = ramo_signal(
                                                p0,Efields,EVecs,mask,
                                                Wfields,WVecs,l,maxlen,interval)[0:6]

fig = go.Figure()
fig.add_trace(go.Scatter(x=time_e/NS*S, y=signal_e,
                    mode='lines', # 'lines', 'markers', 'lines+markers'
                    name="electron",
                    line_shape='linear', # 'spline'
                    line = dict(
                                color='red',
                                width=1,
                                #dash='dot' # 'dash', 'dot', and 'dashdot'
                                ),
                    marker_symbol='circle',
                    marker_line_color="deeppink",
                    marker_color="purple",
                    marker_line_width=2, 
                    marker_size=8,
                    ))
fig.add_trace(go.Scatter(x=time_h/NS*S, y=signal_h,
                    mode='lines', # 'lines', 'markers', 'lines+markers'
                    name="hole",
                    line_shape='linear', # 'spline'
                    line = dict(
                                color='blue',
                                width=1,
                                #dash='dot' # 'dash', 'dot', and 'dashdot'
                                ),
                    marker_symbol='circle',
                    marker_line_color="deeppink",
                    marker_color="purple",
                    marker_line_width=2, 
                    marker_size=8,
                    ))
fig.add_trace(go.Scatter(x=time_s/NS*S, y=signal,
                    mode='lines', # 'lines', 'markers', 'lines+markers'
                    name="signal",
                    line_shape='linear', # 'spline'
                    line = dict(
                                color='black',
                                width=1,
                                #dash='dot' # 'dash', 'dot', and 'dashdot'
                                ),
                    marker_symbol='circle',
                    marker_line_color="deeppink",
                    marker_color="purple",
                    marker_line_width=2, 
                    marker_size=8,
                    ))

fig.update_layout(
    title="SIGNAL",
    xaxis_title="t [ns]",
    xaxis = dict(range=[0,25]),
    yaxis_title="I [A]",
    legend_title="",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)


fig.update_layout(scene = dict(
    ),
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18)
)

fig.write_image("Figures/signal_single_eh.png")
fig.show()
fig.data = [] # Clear fig



#%% Trace

l=0.05 #UM
maxlen = 3000

fig = make_subplots(rows=1, cols=2)
trace_signal = np.zeros(2000)
for iz in range(4, 149):
    print(iz)
    p0 = [18,18,iz]
    time_s,signal = ramo_signal(p0,Efields,EVecs,mask,
                                Wfields,WVecs,l,maxlen,interval)[4:6]
    trace_signal += signal*80*SCALE_5050 #e-h*UM, to mV
    fig.add_trace(go.Scatter(x=time_s/NS*S/SCALE_5050, y=trace_signal,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='black',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ))

fig.update_layout(
    title="Trace signal",
    xaxis_title="t [ns]",
    xaxis = dict(range=[0,30]),
    yaxis_title="I [A]",
    legend_title="",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)

fig.update_layout(scene = dict(
    ),
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18)
)

fig.write_image("Figures/signal_trace.png")
fig.show()
fig.data = [] # Clear fig

print(np.trapz(trace_signal,time_s))
#%% Trace next pixel


WVecs = [Aw+50, Bw+50, Cw] # neighbour pixel  (1,1)
l=0.05 #UM
maxlen = 3000

fig = make_subplots(rows=1, cols=2)
trace_signal = np.zeros(2000)
for iz in range(4, 149):
    print(iz)
    p0 = [18,18,iz]
    time_s,signal = ramo_signal(p0,Efields,EVecs,mask,
                                Wfields,WVecs,l,maxlen,interval)[4:6]
    trace_signal += signal *80*SCALE_5050
    fig.add_trace(go.Scatter(x=time_s/NS*S/SCALE_5050, y=trace_signal,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='black',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ))
fig.add_trace(go.Scatter(x=time_s/NS*S, y=trace_signal,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='red',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=4, 
                        marker_size=8,
                        ))
fig.update_layout(
    title="Trace signal - neighbour pixel (1,1)",
    xaxis_title="t [ns]",
    xaxis = dict(range=[0,30]),
    yaxis_title="I [A]",
    legend_title="",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)
fig.write_image("3.png")
fig.show()
fig.data = [] # Clear fig

print(np.trapz(trace_signal,time_s))
#%% Trace z-capa_lim

WVecs = [Aw, Bw, Cw]
l=0.05 #UM
maxlen = 3000

fig = make_subplots(rows=1, cols=2)
trace_signal = np.zeros(2000)



for iz in range(4, 149):
    print(iz)
    mycolor = ""
    if iz < LAYER_LIM:
        mycolor = "orange"
    elif iz == LAYER_LIM:
        mycolor = "black"
    else:
        mycolor = "blue"
        
    p0 = [18,18,iz]
    time_s,signal = ramo_signal(p0,Efields,EVecs,mask,
                                Wfields,WVecs,l,maxlen,interval)[4:6]
    trace_signal += signal *80*SCALE_5050
    fig.add_trace(go.Scatter(x=time_s/NS*S/SCALE_5050, y=trace_signal,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color=mycolor,
                                    width=1.5,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ))

fig.update_layout(
    title="Trace signal - z study",
    xaxis_title="t [ns]",
    xaxis = dict(range=[0,30]),
    yaxis_title="I [A]",
    legend_title="",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)
fig.write_image("4.png")
fig.show()
fig.data = [] # Clear fig



#%% Trace electron-hole

l=0.05 #UM
maxlen = 3000


trace_electrons = np.zeros(2000)
trace_holes = np.zeros(2000)


for iz in range(4, 149):
    print(iz)    
    p0 = [18,18,iz]
    signal_e, signal_h = ramo_signal(p0,Efields,EVecs,mask,
                                Wfields,WVecs,l,maxlen,interval)[6:8]
    trace_electrons += signal_e *80*SCALE_5050
    trace_holes += signal_h *80*SCALE_5050
    
    
fig = make_subplots(rows=1, cols=2)    
fig.add_trace(go.Scatter(x=time_s/NS*S/SCALE_5050, y=np.log10(trace_electrons+trace_holes+1),
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        fill='tozeroy',
                        line = dict(
                                    color="blue",
                                    width=1.5,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ))
fig.add_trace(go.Scatter(x=time_s/NS*S, y=np.log10(trace_holes+1),
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        fill='tozeroy',
                        line = dict(
                                    color="red",
                                    width=1.5,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ))    

fig.update_layout(
    title="Trace signal e-h",
    xaxis_title="t [ns]",
    xaxis = dict(range=[0,30]),
    yaxis_title="log(I) [A]",
    legend_title="",
    font=dict(
        family="Courier New, monospace",
        size=18,
        color="Black"
    )
)
fig.write_image("5.png")
fig.show()
fig.data = [] # Clear fig

#%% Max frequency band simulation (CIVIDEC)

f_s = 1/time_s[1]
X = fftpack.fft(trace_signal)
freqs = fftpack.fftfreq(len(signal)) * f_s /1e9 # Giga
frequency_cut = 1 # GigaHz

pio.renderers.default = 'browser'
fig = make_subplots(rows=1, cols=2,
                    subplot_titles=("Frequency domain", "Realistic signal"))



fig.add_trace(go.Scatter(x=[frequency_cut,frequency_cut], y=[0,3],
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        showlegend = False,
                        line = dict(
                                    color='black',
                                    width=1,
                                    dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ),row=1, col=1)    
fig.add_trace( go.Scatter(
                        x=[frequency_cut+0.4],
                        y=[2],
                        mode="text",
                        showlegend = False,
                        text=["Band limit"],
                        textposition="top left"
                        ),
                        row=1, col=1
                        )

fig.add_trace(go.Scatter(x=freqs, y=np.log10(np.abs(X)+1),
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='black',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ), row=1, col=1)


for i in range(X.shape[0]):
    if  abs(freqs[i])>frequency_cut: # Frequency cut
        X[i] = 0


        
fig.add_trace(go.Scatter(x=freqs, y=np.log10(np.abs(X)+1),
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='red',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ),row=1, col=1)        



X = fftpack.fft(X)
X = np.roll(np.abs(np.flip(X)), 500)
X = X/np.max(X)*np.max(trace_signal)
fig.add_trace(go.Scatter(x=time_s*1e9-500*50/2000/SCALE_5050, y=X,
                        mode='lines', # 'lines', 'markers', 'lines+markers'
                        name="",
                        line_shape='linear', # 'spline'
                        line = dict(
                                    color='black',
                                    width=1,
                                    #dash='dot' # 'dash', 'dot', and 'dashdot'
                                    ),
                        marker_symbol='circle',
                        marker_line_color="deeppink",
                        marker_color="purple",
                        marker_line_width=2, 
                        marker_size=8,
                        ),row=1, col=2)

# Update xaxis properties
fig.update_xaxes(title_text="frequency [GHz]", range=[0, 3], row=1, col=1)
fig.update_xaxes(title_text="time [ns]", row=1, col=2)
# Update yaxis properties
fig.update_yaxes(title_text="log(F(signal))",range=[0, 3], row=1, col=1)
fig.update_yaxes(title_text="I [A]", row=1, col=2)

fig.write_image("6.png")
fig.show()
fig.data = [] # Clear fig

#%% MAGNETIC FIELDS

p0 = [24.9,5,100]

l=0.05 #UM
maxlen = 3000
BVec = [0,1*T,0] 
interval = 50e-9

[time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN, e_path, h_path
 ] = ramo_magnetic(p0,Efields,EVecs,mask,Wfields,WVecs,BVec,l,maxlen,interval)

pio.renderers.default = 'browser'
fig = go.Figure()
fig.add_trace(go.Scatter3d(x=np.array(e_path[0]), y=np.array(e_path[1]),
                               z=np.array(e_path[2]), marker_size=1,
                                        marker_color="blue",
                                        mode='markers'))
fig.add_trace(go.Scatter3d(x=np.array([-25,25,25,-25,-25]), y=np.array([-25,25,-25,25,-25]),
                               z=np.array([5,5,5,5,5]), marker_size=5,
                                        marker_color="black",
                                        mode='lines'))

# fig.update_layout(
#     scene = dict(
#         xaxis = dict(nticks=4, range=[-25.01,25.01],),
#         yaxis = dict(nticks=4, range=[-25.01,25.01],),
#         zaxis = dict(nticks=4, range=[0,149],),
#         ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=1.5)
    )
fig.update_layout(scene_camera=camera)


fig.show()

#%% MAGNETIC FIELDS row

l=0.05 #UM
maxlen = 3000
BVec = [0*T,3e7*T,0e7*T] 
interval = 50e-9

pio.renderers.default = 'browser'
fig = go.Figure()
for i in np.linspace(-24.99,24.99,20):
    p0 = [i,12,10]
    [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN, e_path, h_path
     ] = ramo_magnetic(p0,Efields,EVecs,mask,Wfields,WVecs,BVec,l,maxlen,interval)
    
    
    fig.add_trace(go.Scatter3d(x=np.array(e_path[0]), y=np.array(e_path[1]),
                                   z=np.array(e_path[2]), marker_size=0.5,
                                            marker_color="blue",
                                            mode='markers'))
    fig.add_trace(go.Scatter3d(x=np.array(h_path[0]), y=np.array(h_path[1]),
                                   z=np.array(h_path[2]), marker_size=0.5,
                                            marker_color="red",
                                            mode='markers'))
for i in np.linspace(-24.99,24.99,20):
    p0 = [i,-12,10]
    [time_e,signal_e,time_h,signal_h,time_s,signal,sig_eN,sig_hN, e_path, h_path
     ] = ramo_magnetic(p0,Efields,EVecs,mask,Wfields,WVecs,BVec,l,maxlen,interval)
    
    
    fig.add_trace(go.Scatter3d(x=np.array(e_path[0]), y=np.array(e_path[1]),
                                   z=np.array(e_path[2]), marker_size=0.5,
                                            marker_color="blue",
                                            mode='markers'))
    fig.add_trace(go.Scatter3d(x=np.array(h_path[0]), y=np.array(h_path[1]),
                                   z=np.array(h_path[2]), marker_size=0.5,
                                            marker_color="red",
                                            mode='markers'))

fig.add_trace(go.Scatter3d(x=np.array([-25,25,25,-25,-25]), y=np.array([-25,25,-25,25,-25]),
                               z=np.array([5,5,5,5,5]), marker_size=5,
                                        marker_color="black",
                                        mode='lines'))

# fig.update_layout(
#     scene = dict(
#         xaxis = dict(nticks=4, range=[-25.01,25.01],),
#         yaxis = dict(nticks=4, range=[-25.01,25.01],),
#         zaxis = dict(nticks=4, range=[0,149],),
#         ))
fig.update_layout()
camera = dict(
    eye=dict(x=0., y=0., z=1.5)
    )
fig.update_layout(scene_camera=camera)


fig.show()





pio.renderers.default = 'browser'
pio.templates.default = "simple_white"

fig = go.Figure()

fig.add_trace(go.Scatter(x=zvec, y=dep_fie[0:disc],
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="Diagonal",
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash='solid'
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))
fig.add_trace(go.Scatter(x=zvec, y=dep_fie[disc:2*disc],
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="Border",
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash='dash'
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))

fig.update_layout(scene = dict(
    ),
    title="Depletion voltage - z dep. - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18),
    xaxis_title="z [um]",
    yaxis_title="V dep. [V]",
)
fig.write_image("Figures/dep_pot_z.png")
fig.show()

fig = go.Figure()

fig.add_trace(go.Scatter(x=np.sqrt(2*xydiag**2), y=dep_fie[2*disc:3*disc],
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="[A]",
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash='solid'
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))
fig.add_trace(go.Scatter(x=xydiag, y=dep_fie[3*disc:4*disc],
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="[B]",
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash='dash'
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))

fig.add_trace(go.Scatter(x=xydiag, y=dep_fie[4*disc:5*disc],
                            mode='lines', # 'lines', 'markers', 'lines+markers'
                            name="[C]",
                            line_shape='linear', # 'spline'
                            line = dict(
                                        color='black',
                                        width=1.5,
                                        dash='dot'
                                        ),
                            marker_symbol='circle',
                            marker_line_color="deeppink",
                            marker_color="purple",
                            marker_line_width=2, 
                            marker_size=8,
                            ))

fig.update_layout(scene = dict(
    ),
    title="Depletion voltage - z=0 - 50x50x150 pixel", autosize=False,
    width=700, height=700,
    margin=dict(l=65, r=50, b=65, t=90),
    font=dict(size=18),
    xaxis_title="l [um]",
    yaxis_title="V dep [V]",
    )
fig.write_image("Figures/dep_pot_z0.png")
fig.show()