"""
This program simulates the inner electric potential of a hybrid pixel detector. 

@author: Cristian Quintana
"""
import numpy as np
import time
import os
import plotly.graph_objects as go
import plotly.io as pio

#%% ---------------------------  Unit system ----------------------------------
#	Longitude
UM = 1.0
MM = 1e3*UM
CM = 10.0*MM
M = 1e6*UM
#	Surface
M2 = M*M
#   Volume 
CM3 = CM**3
#	Charge
EC = 1.0
C = 1.0/1.602176634e-19*EC

#	Mass
KG = 1.0
#	Time
S = 1.0
NS = S*1.0e-9
#	Derived
N = KG*M/S**2.0 #Newton
#


Ksi=8.988e9/12*N*M2/C**2.0 #Coulomb's K relative to silicon


#%% ---------------------------  PARAMETERS -----------------------------------
PPUM = 1.0/UM
R = 4.0*UM # Column radius
SPITCH = 50.0*UM
LPITCH = 50.0*UM
THICK = 150.0*UM
BPTHICK = 3.0*UM # backplane Thickness
TTHICK = THICK+BPTHICK # total Thickness
REF_LIM = 46.0*UM # n col end Thickness
LAYER_LIM = REF_LIM+BPTHICK # array n col end limit

a = SPITCH / (SPITCH*PPUM+1) # Steps, a b c, one for each dimension
b = LPITCH / (LPITCH*PPUM+1)    
c = TTHICK / (TTHICK*PPUM+1)

#%% ---------------------------  Functions ------------------------------------


#%% def gauss Returns the gaussian difussion of a column inside the bulk at a distance "r". "top"= val column/bq, "bot"=val bulk:
def gauss(top, bot, r): 
    """
    Calculate the gaussian diffusion 
    
    gauss() computes the gaussian diffusion of dopants from a top (abs) level
    to the bottom level at a distance "r". Note that if r>5 bot level is auto.
    assigned.

    Parameters
    ----------
    top : float
        Dopant concentration, top level.
    bot : float
        Dopant concentration, bottom level.
    r : float
        DESCRIPTION.

    Returns
    -------
    float
        Dopant concentration by diffusion at "r".

    """
    if r<7: # If r>5 gauss()=bot --> Avoid excesive computing
        return (top-bot) * np.exp(-0.5 * (r/1)**2) + bot 
    else:
        return bot

#%% def dis_charge Returns a mesh of the Charge values an their x, y and z positions:
def dis_charge(
        qMaxN=1.0e20*EC/CM3,
        qMaxP=-1.0e19*EC/CM3,
        qBulk=-1.0e12*EC/CM3,
        qBackPlane=-1.0e19*EC/CM3,
        qP_stop=-5.0e16*EC/CM3):

    """
    Generate a charge distribution according to pixel's parameters.
    
    dis_charge() generates a charge distribution according to pixel's 
    parameters and dopant concentrations. 

    Parameters
    ----------
    qMaxN : float, optional
        Dopant concentration for n+ type columns. The default is 1.0e20*EC/CM3.
    qMaxP : float, optional
        Dopant concentration for p+ type columns. The default is -1.0e19*EC/CM3.
    qBulk : float, optional
        Dopant concentration for bulk. The default is -1.0e12*EC/CM3.
    qBackPlane : float, optional
        Dopant concentration for backplane. The default is -1.0e19*EC/CM3.

    Returns
    -------
    [x,y,z,Charge_dis] 
    x, y, z : 3D float arrays
        Dimension coordinate arrays, 3 dimensions.
    Charge_dis : 3D float array
        Charge distribution.

    """ 
	   
    # Generating pixel's mesh:
    A = np.arange(a/2.0, SPITCH, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
    B = np.arange(b/2.0, LPITCH, b) - LPITCH/2.0
    C = np.arange(c/2.0, TTHICK, c) - BPTHICK

    x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK
    Charge = np.zeros_like(x)

 	# Impurity charge mapping
    dy, dx, dz = x.shape # Mesh dimensions
    
 	# Generating 3 layers to build the pixel
    layer_up = np.zeros((dy, dx)) # Layers upper LAYER_LIM
    layer_lim = np.zeros((dy, dx)) # LAYER_LIM
    layer_down = np.zeros((dy ,dx)) # Layers bottom LAYER_LIM
 	
    
    phi = np.linspace(0, 2*np.pi, 200) # Generating the circunference that defines the column edge
    xcir = R*np.cos(phi)
    ycir = R*np.sin(phi)
 	
 	
    for i in range(dy):
        for j in range(dx):
            xlocal = x[i,j,int(LAYER_LIM)] # Local coords
            ylocal = y[i,j,int(LAYER_LIM)]
            #Comentario
            r1 = np.min(np.sqrt((xcir-xlocal)**2+(ycir-ylocal)**2)) # r to column n
            r2 = np.min(np.sqrt((xcir-(LPITCH/2.0)-xlocal)**2+(ycir-(SPITCH/2.0)-ylocal)**2)) # r to columns p
            r3 = np.min(np.sqrt((xcir-(LPITCH/2.0)-xlocal)**2+(ycir+(SPITCH/2.0)-ylocal)**2))
            r4 = np.min(np.sqrt((xcir+(LPITCH/2.0)-xlocal)**2+(ycir-(SPITCH/2.0)-ylocal)**2))
            r5 = np.min(np.sqrt((xcir+(LPITCH/2.0)-xlocal)**2+(ycir+(SPITCH/2.0)-ylocal)**2))
            rmin = np.amin([r1,r2,r3,r4,r5]); # closer column
            rminpos = np.argmin([r1,r2,r3,r4,r5]); # closer column
            rmin2 = np.amin([r2,r3,r4,r5]); # closer column bottom
            rminpos2 = np.argmin([r2,r3,r4,r5]); # closer column bottom
            
            if rminpos == 0: # Closer column: n
                    colDoping = qMaxN
            else:	# Closer column: p
                    colDoping = qMaxP
            
        		
            layer_up[i,j] = gauss(colDoping, qBulk, rmin)
            layer_lim[i,j] = gauss(colDoping, qBulk, rmin) 
            layer_down[i,j] = gauss(qMaxP, qBulk, rmin2) 
        		
        
            r1 = np.min(np.sqrt((xlocal)**2+(ylocal)**2)) 
            r2 = np.min(np.sqrt((-(LPITCH/2.0)-xlocal)**2+(-(SPITCH/2.0)-ylocal)**2))
            r3 = np.min(np.sqrt((-(LPITCH/2.0)-xlocal)**2+(+(SPITCH/2.0)-ylocal)**2))
            r4 = np.min(np.sqrt((+(LPITCH/2.0)-xlocal)**2+(-(SPITCH/2.0)-ylocal)**2))
            r5 = np.min(np.sqrt((+(LPITCH/2.0)-xlocal)**2+(+(SPITCH/2.0)-ylocal)**2))
            rmin = np.amin([r1,r2,r3,r4,r5]); # closer column
            rminpos = np.argmin([r1,r2,r3,r4,r5]); # closer column
            rmin2 = np.amin([r2,r3,r4,r5]); # closer column bottom
            rminpos2 = np.argmin([r2,r3,r4,r5]); # closer column bottom 
        		
            if rmin < R: # Fill columns
                if rminpos == 0:
                    layer_up[i,j] = qMaxN
                    layer_lim[i,j] = qMaxN
                else:
                    layer_up[i,j] = qMaxP
                    layer_lim[i,j] = qMaxP
            			
        		# Empty columns except 0.5*UM of edge
            if rmin < R-0.5*UM: # Empty
                layer_up[i,j] = 0.0
                layer_lim[i,j] = 0.0
        		
            if rmin2 < R-0.5*UM: # Empty
                layer_down[i,j] = 0.0
                        	
 	# Generating the mesh layer by layer
    for i in range(int(((THICK+BPTHICK)-(LAYER_LIM+1))*PPUM)): 
        Charge[:,:,int((LAYER_LIM+1)*PPUM+i)] = layer_up.copy() 


    for i in range(int(((LAYER_LIM+1)-LAYER_LIM)*PPUM)): # LAYER_LIM
        Charge[:,:,int(LAYER_LIM*PPUM+i)] = layer_lim.copy()


    for i in range(int(LAYER_LIM*PPUM)): 
        Charge[:,:,i] = layer_down.copy()

 	# Completed up to LAYER_LIM

    circleX, circleY = np.meshgrid(np.arange(-4,4,0.1),np.arange(-4,4,0.1)) # Generating the circle that defines the n column bottom
    noUs, dim = circleX.shape
    circuloR = np.sqrt(circleX**2+circleY**2)
    xcir=[]
    ycir=[]
    zcir=[]
    con = 0
    for i in range(dim):
    		for j in range(dim):
        		if circuloR[i,j] <= 4.0: # Defining the circle's points
             			con = con + 1
             			xcir = np.append(xcir.copy(), circleX[i,j].copy())
             			ycir = np.append(ycir.copy(), circleY[i,j].copy())
             			zcir = np.append(zcir.copy(), REF_LIM-c) # End of n column
        
    dim1, dim2, dim3 = Charge.shape
    for i in range(dim1):
        for j in range(dim2):
            for k in range(int(LAYER_LIM*PPUM)):
                xlocal = x[i,j,k] # Local positions
                ylocal = y[i,j,k]
                zlocal = z[i,j,k]
                r1=zlocal # Distance to the base
                r2=np.min(np.sqrt((xcir-xlocal)**2+(ycir-ylocal)**2+(zcir-zlocal)**2)) # Distance to circle in LAYER_LIM
                if r1<r2:
                    Qlocal=gauss(qMaxP,qBulk,r1) # Assign gauss value
                    if (np.abs(Qlocal)>np.abs(Charge[i,j,k])) & (r1<10): # Only if the absolute value is higher and r is low
                        Charge[i,j,k]=Qlocal
                    
                else: 
                    Qlocal=gauss(qMaxN,qBulk,r2) # Assign gauss value (circle)
                    if (np.abs(Qlocal)>np.abs(Charge[i,j,k])) & (r2<10): 
                        Charge[i,j,k]=Qlocal
                        
    # P-stop -> pst
    pst_mr = 20*UM
    pst_thickness = 5*UM
    pst_depth = 4*UM
       
    circleX, circleY, circleZ = np.meshgrid(np.arange(-pst_mr,pst_mr,0.5),
                                   np.arange(-pst_mr,pst_mr,0.5),
                                   np.arange(THICK-pst_depth,THICK,0.5)) 
    # Generating the ring that defines the P-stop 
    noUs, dim1, dim3 = circleX.shape
    circuloR = np.sqrt(circleX**2+circleY**2)
    xcir=[]
    ycir=[]
    zcir=[]
    for i in range(dim1):
        for j in range(dim1):
            for k in range(dim3):
                if circuloR[i,j,k] <= pst_mr and \
                    circuloR[i,j,k] >= (pst_mr-pst_thickness) : # Defining the rings's points
                 			xcir = np.append(xcir.copy(), circleX[i,j,k].copy())
                 			ycir = np.append(ycir.copy(), circleY[i,j,k].copy())
                 			zcir = np.append(zcir.copy(), circleZ[i,j,k].copy()) # End of n column
       
    dim1, dim2, dim3 = Charge.shape                         		
    for i in range(dim1): # Doping the distribution with the ring
        for j in range(dim2):
            for k in range(int((TTHICK-pst_depth-6)*PPUM),dim3):
                xlocal = x[i,j,k] # Local positions
                ylocal = y[i,j,k]
                zlocal = z[i,j,k]
                r=np.min(np.sqrt((xcir-xlocal)**2+(ycir-ylocal)**2+(zcir-zlocal)**2)) # Distance to circle in LAYER_LIM
                Qlocal = gauss(qP_stop,qBulk,r)
                if np.abs(Charge[i,j,k]) > 0:
                    Charge[i,j,k]=Charge[i,j,k]+Qlocal

    

    for i in range(int(3*PPUM)): # Backplane to qMaxP
        Charge[:,:,i]=(layer_down.copy()*0.0)+qMaxP
        
        
    	
    return [x,y,z,(Charge/PPUM**3.0)] # Return charge distribution (positions and values), PPUM volume correction
    	
    	
    	
#%% def potential

def potential(xmesh,ymesh,zmesh,x,y,z,Charge):
    """
    Compute the electric potential
    
    potential() computes the electrical potential generated in the coordinates
    "xmesh","ymesh","zmesh" by a charge distribution "Charge" in the "x", "y",
    "z" coords.
    
    Parameters
    ----------
    xmesh, ymesh, zmesh : 3D float arrays
        Arrays with the 3 dimensional coords in which the potential is generated.
    x, y, z : 3D float arrays
        Dimension coordinate arrays where "Charge" is placed, 3 dimensions.
    Charge : 3D float array
        Charge distribution to generate the potential.

    Returns
    -------
    potPoint : 3D float array
        Potential distribution.

    """
    
    dim1,dim2,dim3=xmesh.shape
    potPoint=np.zeros_like(xmesh)
    count=0
    first_time = time.time() 
    for i in range(dim1):
        count=count+1 #Progress bar
        current_time = time.time()-first_time
        exp_time=(dim1)*current_time/count
        print("-----------------------------------") 
        print("--- Current time  %s min ---" % ((current_time)/60.0)) 
        print("--- Expected time %s min ---" % ((exp_time)/60.0)) 
        print("--- Left time     %s min ---" % ((exp_time-current_time)/60.0)) 
        for j in range(dim2):  
            for k in range(dim3):
                r = np.sqrt((x-xmesh[i,j,k])**2+(y-ymesh[i,j,k])**2+(z-zmesh[i,j,k])**2)
                #potPoint[i,j,k]=np.sum(-(1-np.exp(-100*r[r>0]**1.4))*Ksi*Charge[r>0]/r[r>0]) 
                potPoint[i,j,k]=np.sum(Ksi*Charge[r>0]/r[r>0]) # Low PPUM -> No sigmoid needed
                
                
    return potPoint

#%% Returns a mesh of the Charge values an their x, y and z positions:
def electric_field(pixel_pot):
	
	# Generating pixel's mesh:
    A = np.arange(a/2.0-a, SPITCH+a, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
    B = np.arange(b/2.0-b, LPITCH+b, b) - LPITCH/2.0
    C = np.arange(c/2.0, TTHICK+c, c) - BPTHICK

    x_ev, y_ev, z_ev = np.meshgrid(B, A, C) # Mesh with extra layer for derivate pot.
    pot_ev = x_ev.copy()*0
    
    dim1,dim2,dim3 = np.shape(pixel_pot)
    print(dim3)
    for i in range(0, dim1):
        for j in range(0, dim2):
            for k in range(0, dim3):
                pot_ev[i+1][j+1][k] = pixel_pot[i][j][k] 
    #Walls
    pot_ev[0,:,:]=pot_ev[1,:,:]
    pot_ev[dim1+1,:,:]=pot_ev[dim1,:,:]
    pot_ev[:,0,:]=pot_ev[:,1,:]
    pot_ev[:,dim2+1,:]=pot_ev[:,dim2,:]
    pot_ev[:,:,dim3]=pot_ev[:,:,dim3-1]
    
    efield_x,efield_y,efield_z=np.gradient(pot_ev, a, b, c)
    print(efield_x.shape)
    return [efield_x[1:dim1+1,1:dim2+1,0:dim3], efield_y[1:dim1+1,1:dim2+1,0:dim3], efield_z[1:dim1+1,1:dim2+1,0:dim3]] 
        
    	
	
#%% ----------------------------> MAIN (On progress) <--------------------------------------------------------	
	    	
# Generating gauss-like charge distribution
print("Generating harge distribution")
X,Y,Z,Charge=dis_charge()    
print("Charge distribution generated")

#%% Potentials
print("Init potential by cells")
arrayPot00=potential(X,Y,Z,X,Y,Z,Charge)#(0,0)
arrayPot10=potential(X,Y,Z,X+50.0,Y,Z,Charge) #(1,0)
arrayPot11=potential(X,Y,Z,X+50.0,Y+50.0,Z,Charge) #(1,1)
print("Potential cells DONE")  

#%% Generatin the mesh contribution
arrayPotSum = arrayPot00.copy()

arrayPotSum = arrayPotSum + \
    arrayPot10.copy() + \
        np.rot90(arrayPot10.copy(), 1) + \
            np.rot90(arrayPot10.copy(), 2) + \
                np.rot90(arrayPot10.copy(), 3)
            
arrayPotSum = arrayPotSum + \
    arrayPot11.copy() + \
        np.rot90(arrayPot11.copy(), 1) + \
            np.rot90(arrayPot11.copy(), 2) + \
                np.rot90(arrayPot11.copy(), 3)
                
print("FULL Potential DONE")  
# reshaping the array from 3D
# matrice to 2D matrice.                
arrayPotSum2D = arrayPotSum.reshape(arrayPotSum.shape[0], -1)
# saving reshaped array to file.
np.savetxt("arrayPotSum_5050.txt", arrayPotSum2D)
print('Saved')
#%% Load Potential saved
# X Y Z are needed
# loaded_arr = np.loadtxt("arrayPotSum_5050.txt")

# arrayPotSum = loaded_arr.reshape(
#     loaded_arr.shape[0], loaded_arr.shape[1] // X.shape[2], X.shape[2])

#%% Electric field from 9x9 pixel matrix

efield_x,efield_y,efield_z = electric_field(arrayPotSum)
print("FULL E-Field DONE")  

#%% Plot isosurfaces

            
arrayPot_units=arrayPotSum.copy()
efield_x,efield_y,efield_z=np.gradient(arrayPot_units, a, b, c)
efield_magnitude=np.sqrt(efield_x**2+efield_y**2+efield_z**2)*1e0 # to v/cm
maxVal=np.max(efield_magnitude)
minVal=np.min(efield_magnitude)

pio.renderers.default = 'browser'
fig = go.Figure(data=go.Isosurface(
    x=X.flatten(),
    y=Y.flatten(),
    z=Z.flatten(),
    value=efield_magnitude.flatten(),
    isomin=minVal,
    isomax=maxVal,
    colorbar_title="E <br> [V/cm]",
    surface_count=7,
    opacity=0.6,
    caps=dict(x_show=False, y_show=False)
    ))

fig.update_layout(scene = dict(
    zaxis = dict(nticks=4, range=[-2.5,149.5],)),
    title="E field magnitude - 50x50x150 pixel", autosize=False,
    width=800, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)


# duration = 10  # seconds
# freq = 440  # Hz
# os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))



#%% Carge isosurfaces

chargemap=[[0.0, "rgb(165,0,38)"],
                [0.1111111111111111, "rgb(215,48,39)"],
                [0.2222222222222222, "rgb(244,109,67)"],
                [0.3333333333333333, "rgb(253,174,97)"],
                [0.4444444444444444, "rgb(254,224,144)"],
                [0.5555555555555556, "rgb(224,243,248)"],
                [0.6666666666666666, "rgb(171,217,233)"],
                [0.7777777777777778, "rgb(116,173,209)"],
                [0.8888888888888888, "rgb(69,117,180)"],
                [1.0, "rgb(49,54,149)"]]


repCharge = np.sign(Charge)*np.log(np.abs(Charge))
maxVal=np.max(repCharge)
minVal=np.min(repCharge)
pio.renderers.default = 'browser'
fig = go.Figure(data=go.Isosurface(
    x=X.flatten(),
    y=Y.flatten(),
    z=Z.flatten(),
    value=repCharge.flatten(),
    colorscale=chargemap,
    colorbar_title="Doping <br> log(i/cm3)",
    isomin=-19,
    isomax=20,
    surface_count=15,
    opacity=0.7,
    caps=dict(x_show=False, y_show=False)
    )
    )

fig.update_layout(scene = dict(
    zaxis = dict(nticks=4, range=[-2.5,149.5],)),
    title="Doping distribution - 50x50x150 pixel", autosize=False,
    width=800, height=700,
    margin=dict(l=65, r=50, b=65, t=90)
)
    
fig.show()


# duration = 10  # seconds
# freq = 440  # Hz
# os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))




























