#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Weighting field 50x50x150um pixel detector

@author: cristian
"""
#%% ---------------------------  Imports --------------------------------------

import numpy as np
import os
import time
import plotly.graph_objects as go

#%% ---------------------------  Unit system ----------------------------------
#	Longitude
UM = 1.0
MM = 1e3*UM
CM = 10.0*MM
M = 1e6*UM
#	Surface
M2 = M*M
#   Volume 
CM3 = CM**3
#	Charge
EC = 1.0
C = 1.0/1.602176634e-19*EC
#	Mass
KG = 1.0
#	Time
S = 1.0
NS = S*1.0e-9
#	Derived
N = KG*M/S**2.0 #Newton
#
Ksi=8.988e9/12*N*M2/C**2.0 #Coulomb's K relative to silicon

#%% ---------------------------  PARAMETERS ---------------------------------------------
PPUM = 1.0/UM
R = 4.0*UM # Column radius
SPITCH = 25.0*UM
LPITCH = 100.0*UM
THICK = 150.0*UM
BPTHICK = 3.0*UM # backplane Thickness
TTHICK = THICK+BPTHICK # total Thickness
REF_LIM = 46.0*UM # n col end Thickness
LAYER_LIM = REF_LIM+BPTHICK # array n col end limit

a = SPITCH / (SPITCH*PPUM+1) # Steps, a b c, one for each dimension
b = LPITCH / (LPITCH*PPUM+1)    
c = TTHICK / (TTHICK*PPUM+1)

#%% ---------------------------  Functions ------------------------------------
def dif_i(i, j, k, w_pot):
    """
    Compute the theoretical potential difference (residual) of a point.
    
    dif() computes the residual of a given point defined by its indexes.

    Parameters
    ----------
    i, j, k : int
        Indexes of the point inside "w_pot".
    w_pot : 3D float array
        Weighting potential distribution.

    Returns
    -------
    float
        Difference in potential.

    """
    w_dif_i=(1/6) * ( \
                     w_pot[i+1,j,k] + \
                         w_pot[i-1,j,k] + \
                             w_pot[i,j+1,k] + \
                                 w_pot[i,j-1,k] + \
                                     w_pot[i,j,k+1] + \
                                         w_pot[i,j,k-1]) -  w_pot[i,j,k]
    return abs(w_dif_i)
                         
def corr_i(i, j, k, w_pot):
    """
    Compute the theoretical potential of a point.
    
    corr_i() computes the theoretical potential of a given point (defined by its
    indexes) to reduce the maximum difference.

    Parameters
    ----------
    i, j, k : int
        Indexes of the point inside "w_pot".
    w_pot : 3D float array
        Weighting potential distribution.

    Returns
    -------
    float
        New potential value.

    """
    corr_i=(1/6) * ( \
                     w_pot[i+1,j,k] + \
                         w_pot[i-1,j,k] + \
                             w_pot[i,j+1,k] + \
                                 w_pot[i,j-1,k] + \
                                     w_pot[i,j,k+1] + \
                                         w_pot[i,j,k-1]) 
    return corr_i

def corr_i_b(i, j, k, w_pot):
    """
    Compute the theoretical potential of a point in surface (k=zmax).
    
    corr_i_b() computes the theoretical potential of a given point (defined by its
    indexes) to reduce the maximum difference in surface.

    Parameters
    ----------
    i, j, k : int
        Indexes of the point inside "w_pot".
    w_pot : 3D float array
        Weighting potential distribution.

    Returns
    -------
    float
        New potential value (surface).

    """
    corr_i_b=(1/5) * ( \
                     w_pot[i+1,j,k] + \
                         w_pot[i-1,j,k] + \
                             w_pot[i,j+1,k] + \
                                 w_pot[i,j-1,k] + \
                                     w_pot[i,j,k-1]) 
    return corr_i_b


def weight_pot(cells_x, cells_y):
    """
    Save and return a data file/array containing the weihing potential mesh.
    
    weight_pot() generates a data file with the calculations of the weighting
    fild. It takes the values of Parameters section to define the mesh and
    columns, backplane, etc. If there is an existing file this function uses
    it as start point. "past" parameter is used to stop the iterative process
    as an approximate running time. During the execution the current state of
    w_pot is saved and a image file (generic folder) to verify the progress 
    every 10 min approx..
    
    Parameters
    ----------
    cells_x , cells_y : int
        Dimension in number of cells simulated for each direction. 

    Returns
    -------
    w_pot : 3D float array
        Weighting potential distribution.

    """
    # Save figures configuration:
    figures_path = "Figures/Weighting_pot_" + (str(np.int64(SPITCH)) + "x" + \
                                               str(np.int64(LPITCH)) + "x" + \
                                                   str(np.int64(THICK)) + "/" + \
                                                       "GenerationEvo/")
    
    if not os.path.exists(figures_path):
        os.makedirs(figures_path)
        print("Dirs made -->  " + figures_path)
        
    con_figs = len([name for name in os.listdir(figures_path) if os.path.isfile(os.path.join(figures_path, name))])
    if con_figs > 0: # If any figure, always E fig_last
        con_figs -= 1
        
    # Save data configuration
    data_textfile = "weighting_pot_" + (str(np.int64(SPITCH)) + "x" + \
                                               str(np.int64(LPITCH)) + "x" + \
                                                   str(np.int64(THICK)) + ".txt")
    
	# Generating pixel's mesh:
    A = np.arange(a/2.0-SPITCH*cells_y, SPITCH+SPITCH*cells_y, a) - SPITCH/2.0 # Vectors, A B C, one for each dim
    B = np.arange(b/2.0-LPITCH*cells_x, LPITCH+LPITCH*cells_x, b) - LPITCH/2.0
    C = np.arange(c/2.0, TTHICK, c) - BPTHICK

    x, y, z = np.meshgrid(B, A, C) # Mesh x-y centered and z0=-BPTHICK
    
    # 3D mesh variables
    mask = np.ones_like(x) # Mask CC points
    w_pot =  np.zeros_like(x) # Weighting potential
    w_dif =  np.zeros_like(x) # Iterative differences
    # Other algorithm-based variables
    matrix_dif = w_dif[:,:,0].copy()
    vector_dif = matrix_dif[:,0].copy()
    max_dif = 0 # Current Max difference
    
    # matrix_dif_in = w_dif[:,:,0].copy()
    # vector_dif_in  = matrix_dif[:,0].copy()
    # max_dif_in  = 0 # Current Max difference
    
    I = -1 # Indexes for max
    J = -1
    K = -1
    
    # Generating initial potential
    
    #Defining mask
    for nx in range (-cells_x, cells_x+1): # Adding columns (n)
        for ny in range (-cells_y, cells_y+1):
            px = nx*LPITCH
            py = ny*SPITCH
            r = np.sqrt((px-x)**2+(py-y)**2)
            rmask = (r > R).astype(int)+(z < REF_LIM).astype(int)
            rmask = (rmask > 0).astype(int)
            mask = mask*rmask
        
    for nx in range (-cells_x-1, cells_x+2): # Adding columns (p)
        for ny in range (-cells_y-1, cells_y+2):
            px = nx*LPITCH+LPITCH/2
            py = ny*SPITCH+SPITCH/2
            r = np.sqrt((px-x)**2+(py-y)**2)
            rmask = (r > R).astype(int)
            mask = mask*rmask
            
    mask = mask*(z > 0).astype(int) * (z < THICK-c).astype(int)
    mask = mask*(x <= a).astype(int) #sim conditions
    mask = mask*(y <= b).astype(int) #sim conditions
  
    #Defining initial potential or taking saved one
    try: # Load
        loaded_arr = np.loadtxt(data_textfile)
        w_pot = loaded_arr.reshape(
            loaded_arr.shape[0], loaded_arr.shape[1] // x.shape[2], x.shape[2])
        print('File load')
    except: # Generate
        r = np.sqrt((x)**2+(y)**2)
        w_pot = (r <= R).astype(int)+(z >= REF_LIM).astype(int)
        w_pot = (w_pot > 1).astype(int)
        w_pot = np.double(w_pot)
        print('File not load -> initial potential defined ' )

    r = np.sqrt((x)**2+(y)**2)
    rmask = (r <= R).astype(int)+(z >= REF_LIM).astype(int)
    mask = mask*(rmask <= 1).astype(int)
    
    print("Full maximum difference search started")
    # Algorithm -> fast search
    for i in range(1, w_pot.shape[0]-1):  # Full search
        for j in range(1, w_pot.shape[1]-1):
            for k in range(3, w_pot.shape[2]-1):
                w_dif[i,j,k] = dif_i(i, j, k, w_pot)
    w_dif = w_dif*mask # Mask <- c.c.
    
    for i in range(1, w_pot.shape[0]-1): # to matrix
        for j in range(1, w_pot.shape[1]-1):
            matrix_dif[i,j] = np.max(w_dif[i,j,:])
            
    for i in range(1, w_pot.shape[0]-1): # to vector
            vector_dif[i] = np.max(matrix_dif[i,:])  

    max_dif =  np.max(vector_dif) # to value
    
    con = 0 # n iterations
    past = 8 #1=1h  RUNNING tIME
    pascon = 1e6 # stop wih n iterations (only init value, >>, 1st iter.)
    t_laps = time.time() # Time control
    dim1 = w_pot.shape[0]
    dim2 = w_pot.shape[1]
    dim3 = w_pot.shape[2]
    
    lim1 = np.int64((dim1+1)/2) # Sim max search lim
    lim2 = np.int64((dim2+1)/2)
    
    print("Iterative process started")
    #while con<pascon:
    while True:
        con += 1
        I = np.argmax(vector_dif[:lim1])  # Find max dif in sym
        J = np.argmax(matrix_dif[I,:lim2])
        K = np.argmax(w_dif[I,J,:])
        
        if K < (dim3-2):
            w_pot[I,J,K] = corr_i(I, J, K, w_pot)
            w_dif[I,J,K] = 0
            # Recover max dif
            iloc = I+1
            w_dif[iloc,J,K] = dif_i(iloc, J, K, w_pot) * mask[iloc,J,K]
            if w_dif[iloc,J,K]>matrix_dif[iloc,J]:
                matrix_dif[iloc,J] = w_dif[iloc,J,K]
                
            iloc = I-1
            w_dif[iloc,J,K] = dif_i(iloc, J, K, w_pot) * mask[iloc,J,K]
            if w_dif[iloc,J,K]>matrix_dif[iloc,J]:
                matrix_dif[iloc,J] = w_dif[iloc,J,K]

            iloc = J+1
            w_dif[I,iloc,K] = dif_i(I, iloc, K, w_pot) * mask[I,iloc,K]
            if w_dif[I,iloc,K]>matrix_dif[I,iloc]:
                matrix_dif[I,iloc] = w_dif[I,iloc,K]

            iloc = J-1
            w_dif[I,iloc,K] = dif_i(I, iloc, K, w_pot) * mask[I,iloc,K]
            if w_dif[I,iloc,K]>matrix_dif[I,iloc]:
                matrix_dif[I,iloc] = w_dif[I,iloc,K]
            
            iloc = K+1
            w_dif[I,J,iloc] = dif_i(I, J, iloc, w_pot) * mask[I,J,iloc]

            iloc = K-1
            w_dif[I,J,iloc] = dif_i(I, J, iloc, w_pot) * mask[I,J,iloc]


            matrix_dif[I,J] = np.max(w_dif[I,J,:])
            for i in range(I-1, I+1):
                vector_dif[i] = np.max(matrix_dif[i,:])  
            max_dif =  np.max(vector_dif)
            
        elif K == (dim3-2):
            w_pot[I,J,K] = corr_i_b(I, J, K, w_pot)
            w_dif[I,J,K] = 0
            w_pot[I,J,K+1] = w_pot[I,J,K] # CC
            # Recover max dif
            iloc = I+1
            w_dif[iloc,J,K] = dif_i(iloc, J, K, w_pot) * mask[iloc,J,K]
            if w_dif[iloc,J,K]>matrix_dif[iloc,J]:
                matrix_dif[iloc,J] = w_dif[iloc,J,K]
                
            iloc = I-1
            w_dif[iloc,J,K] = dif_i(iloc, J, K, w_pot) * mask[iloc,J,K]
            if w_dif[iloc,J,K]>matrix_dif[iloc,J]:
                matrix_dif[iloc,J] = w_dif[iloc,J,K]

            iloc = J+1
            w_dif[I,iloc,K] = dif_i(I, iloc, K, w_pot) * mask[I,iloc,K]
            if w_dif[I,iloc,K]>matrix_dif[I,iloc]:
                matrix_dif[I,iloc] = w_dif[I,iloc,K]

            iloc = J-1
            w_dif[I,iloc,K] = dif_i(I, iloc, K, w_pot) * mask[I,iloc,K]
            if w_dif[I,iloc,K]>matrix_dif[I,iloc]:
                matrix_dif[I,iloc] = w_dif[I,iloc,K]
            
            iloc = K-1
            w_dif[I,J,iloc] = dif_i(I, J, iloc, w_pot) * mask[I,J,iloc]


            matrix_dif[I,J] = np.max(w_dif[I,J,:])
            for i in range(I-1, I+1):
                vector_dif[i] = np.max(matrix_dif[i,:])  
            max_dif =  np.max(vector_dif) 
        
        # Sim relations    
        Isim = dim1-1-I
        Jsim = dim2-1-J    
        
        w_pot[Isim,J,K] = w_pot[I,J,K]
        w_pot[I,Jsim,K] = w_pot[I,J,K] 
        w_pot[Isim,Jsim,K] = w_pot[I,J,K] 
            
        if con % 1e5 == 0:
            itt = 1e5/(time.time()-t_laps)
            print("-----------------------------------") 
            print("--- Iterations/s= %s ---" % (itt) )
            print("---  Left time = %s min---" % ((pascon-con)/itt/60))
            print("--- Index values = %s %s %s ---" % (I, J, K )) 
            print("--- Max dif  =  %s  ---" % (max_dif))
            t_laps = time.time()
            pascon = past*3600*itt
            
            if con % 1e7 == 0: # Save fig and save data (~ each 10min)
                # Save fig
                con_figs += 1
                fig = go.Figure(data=[go.Surface(z=np.log(w_pot[:,:,100]+1))])
                fig_title = "Max difference = " + str(max_dif) + " [V]"
                fig.update_layout(title=fig_title, autosize=True,
                                  margin=dict(l=65, r=50, b=65, t=90))
                fig.update_layout(
                    scene = dict(
                        zaxis = dict(nticks=4, range=[1e-7,1.01],),))
                camera = dict(
                    eye=dict(x=0., y=0., z=0.7)
                    )
                fig.update_layout(scene_camera=camera)
                fig.write_image(figures_path+"fig_"+str(np.int8(con_figs))+".png")
                fig.write_image(figures_path+"fig_last.png") # Last image to upload
                fig.data = [] # Clear fig
                
                # Save data
                array2D = w_pot.reshape(w_pot.shape[0], -1)
                np.savetxt(data_textfile, array2D)
    # Final save data
    array2D = w_pot.reshape(w_pot.shape[0], -1)
    np.savetxt(data_textfile, array2D)
    print('Finish')
    return w_pot




#%% ---------------------------  RUN  -----------------------------------------

#w_pot = weight_pot(2,2) # 50x50
w_pot = weight_pot(2,7) # 25x100